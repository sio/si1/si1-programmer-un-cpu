START
MVA @011     ;on place la valeur 4 dans A
MVB 4        ;on place la valeur 4 dans B
ADD          ;on ajoute A et B => R=A=4+4=8
ADD          ;on ajoute A et B => R=A=8+4=12
STO @111     ;on stocke la valeur 12 dans @111
MVA @010     ;A=2
OPP          ;R=A=-2
MVB @111     ;B=12 (12=4x3)
ADD          ;R=A=-2+12=10
STO @111     ;on place le resultat final dans @111
END

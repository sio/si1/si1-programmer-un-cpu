START
MVA 13       ;on place la valeur 13 dans A
MVB 13       ;on place la valeur 13 dans B
ADD          ;on ajoute A et B => 13*2=26
STO @111     ;on stocke la valeur 26 dans @111
MVB @111     ;B=26
ADD          ;R=A=26+26=52
STO @111     ;on stocke la valeur 52 dans @111
MVA 12       ;A=12
OPP          ;A=-A=-12
MVB 6        ;B=6
ADD          ;R=A=-12+6=-6
MVB 3        ;B=3
ADD          ;R=A=-6+3=-3
MVB @111     ;B=52
ADD          ;R=A=-3+52=49
MVB -31      ;B=-31
ADD          ;R=A=49+(-31)=18
STO @111     ;on place le resultat final dans @111
END
